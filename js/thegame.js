const config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                y: 200
            }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

const game = new Phaser.Game(config);
let player = null;
let keys = null;

function preload() {
    this.load.setBaseURL('http://bracket.bashmagic.com');
    this.load.image('player', 'assets/player.png');
}

function create() {
    keys = this.input.keyboard.createCursorKeys();

    player = this.physics.add.image(100, 100, 'player');
    player.setBounce(0.2);
    player.setCollideWorldBounds(true);
    player.body.setGravityY(300);
}

function update() {
    if (keys.left.isDown) {
        player.setVelocityX(-160);
    } else if (keys.right.isDown) {
        player.setVelocityX(160);
    } else {
        player.setVelocityX(0);
    }

    if (keys.up.isDown && player.body.touching.down) {
        player.setVelocityY(-330);
    }
}